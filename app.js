const express = require('express')
const app = express()
const path = require('path');
const createError = require('http-errors');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const expressValidator = require('express-validator');
const flash = require('express-flash');
const session = require('express-session');
const bodyParser = require('body-parser');
const mysql = require('mysql2')
const connection  = require('./db/db');
var authRouter = require('./views/router/authentification');


const port = 4003

app.set('view engine', 'ejs')
app.use(express.static(path.join(__dirname, '/views')));
app.use("/css",express.static(path.join(__dirname, "/css")))
app.use("/js",express.static(path.join(__dirname, "/js")))
app.use("/scss", express.static(path.join(__dirname, "/scss")))
app.use("/images", express.static(path.join(__dirname, "/images")))

const user = {
    firstName: 'Tim',
    lastName: 'Cook',
}
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({ 
  secret: '123456cat',
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 60000 }
}))
app.use(flash());
app.use(expressValidator());


app.use('/', authRouter);

app.use(function(req, res, next) {
  next(createError(404));
});
 
//error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
 
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


app.listen(port, () => {
  console.log(`App listening at port ${port}`)
})


module.exports = app;